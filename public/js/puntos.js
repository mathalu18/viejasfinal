(function (yourCode) {
    yourCode(window.jQuery, window, document)
}(function ($, window) {
    let $empleados, $puntos, $tabla
    const url = window.location.origin + '/'

    $(function () {
        init()
        format()
    })

    function init() {
        $empleados = $('#empleado')
        $puntos = $('.puntos')
        $tabla = $('.table')
        $.ajax({
            url: url + 'procesar-puntos-empleados',
            success: function (result) {
                let data = JSON.parse(result)
                let html
                $.each(data, function (key, item) {
                    if (key != 'promedio') {
                        html += '<tr>' +
                            '<td>' + item.empleado + '</td>' +
                            '<td>' + item.descripcion + '</td>' +
                            '<td>' + item.monto_base + '</td>' +
                            '<td>' + item.ventas + '</td>' +
                            '<td>' + item.porcentaje + '</td>' +
                            '<td>' + item.puntos + '</td>' +
                            '</tr>'
                    } else {
                        $('.resultado').text(item)
                    }
                })
                $('.table tbody').append(html)
            }
        })
    }

    function format() {
        $tabla.DataTable();
        $('.table thead').css({'background-color': '#337ab7', 'color': 'white'})
    }
}))