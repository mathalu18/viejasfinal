<?php

namespace App\Entity;

use App\Repository\VentaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VentaRepository::class)
 */
class Venta
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_empleado;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_periodo;

    /**
     * @ORM\Column(type="bigint")
     */
    private $valor_venta;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdEmpleado(): ?int
    {
        return $this->id_empleado;
    }

    public function setIdEmpleado(int $id_empleado): self
    {
        $this->id_empleado = $id_empleado;

        return $this;
    }

    public function getIdPeriodo(): ?int
    {
        return $this->id_periodo;
    }

    public function setIdPeriodo(int $id_periodo): self
    {
        $this->id_periodo = $id_periodo;

        return $this;
    }

    public function getValorVenta(): ?string
    {
        return $this->valor_venta;
    }

    public function setValorVenta(string $valor_venta): self
    {
        $this->valor_venta = $valor_venta;

        return $this;
    }
}
