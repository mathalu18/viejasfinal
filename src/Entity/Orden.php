<?php

namespace App\Entity;

use App\Repository\OrdenRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrdenRepository::class)
 */
class Orden
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_empleado;

    /**
     * @ORM\Column(type="bigint")
     */
    private $total_orden;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $estado;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdEmpleado(): ?int
    {
        return $this->id_empleado;
    }

    public function setIdEmpleado(int $id_empleado): self
    {
        $this->id_empleado = $id_empleado;

        return $this;
    }

    public function getTotalOrden(): ?string
    {
        return $this->total_orden;
    }

    public function setTotalOrden(string $total_orden): self
    {
        $this->total_orden = $total_orden;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }
}
