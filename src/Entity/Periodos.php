<?php

namespace App\Entity;

use App\Repository\PeriodosRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PeriodosRepository::class)
 */
class Periodos
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="bigint")
     */
    private $monto_base;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getMontoBase(): ?string
    {
        return $this->monto_base;
    }

    public function setMontoBase(string $monto_base): self
    {
        $this->monto_base = $monto_base;

        return $this;
    }
}
