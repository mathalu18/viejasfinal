<?php

namespace App\Repository;

use App\Entity\ProductoOrden;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductoOrden|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductoOrden|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductoOrden[]    findAll()
 * @method ProductoOrden[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductoOrdenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductoOrden::class);
    }

    // /**
    //  * @return ProductoOrden[] Returns an array of ProductoOrden objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductoOrden
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
