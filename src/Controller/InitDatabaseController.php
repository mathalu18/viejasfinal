<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class InitDatabaseController extends AbstractController
{
    /**
     * @Route("/init-database", name="init_database")
     */
    public function index()
    {


        $this->crearTabla();
        return $this->render('init_database/index.html.twig', [
            'controller_name' => 'InitDatabaseController',
        ]);;
    }

    private function crearTabla()
    {

        $array = array(
            "CREATE TABLE orden (id INT AUTO_INCREMENT NOT NULL, id_empleado INT NOT NULL, total_orden BIGINT NOT NULL, estado VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB",
            "CREATE TABLE periodos (id varchar(255) NOT NULL, descripcion VARCHAR(255) NOT NULL, monto_base BIGINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB",
            "CREATE TABLE producto_orden (id INT AUTO_INCREMENT NOT NULL, id_orden INT NOT NULL, id_producto INT NOT NULL, cantidad INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB",
            "CREATE TABLE venta (id INT AUTO_INCREMENT NOT NULL, id_empleado INT NOT NULL, id_periodo INT NOT NULL, valor_venta BIGINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB",
            "CREATE TABLE empleado (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, apellido VARCHAR(255) NOT NULL, fecha_nacimiento DATE NOT NULL, usuario VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, rol VARCHAR(255) NOT NULL, id_jefe INT NOT NULL, admin TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB",
            "CREATE TABLE producto (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, descripcion VARCHAR(255) NOT NULL, url_imagen VARCHAR(255) NOT NULL, puntos INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB",
            "CREATE TABLE sesion (id, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB"
        );

        $servername = "localhost";
        $username = "admin";
        $password = "admin";
        $database = "incentivos";
        $conn = new \mysqli($servername, $username, $password, $database);

        foreach ($array as $valor) {
            $this->executeQuery($valor);
        }
    }

    private function executeQuery($sql)
    {
        $servername = "localhost";
        $username = "admin";
        $password = "admin";
        $database = "incentivos";
        $conn = new \mysqli($servername, $username, $password, $database);
        return $conn->query($sql);
    }

}
