<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class EmpleadosController extends AbstractController
{
    /**
     * @Route("/empleados", name="empleados")
     */
    public function index()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $this->agregarEmpleados($data);
        return $this->render('empleados/index.html.twig', [
            'controller_name' => 'EmpleadosController',
        ]);
    }

    private function agregarEmpleados($empleados)
    {

        $servername = "localhost";
        $username = "admin";
        $password = "admin";
        $database = "incentivos";
        $conn = new \mysqli($servername, $username, $password, $database);
        $this->executeQuery('TRUNCATE TABLE `empleado`');
        foreach ($empleados as $empleado) {
            $sql  = "INSERT INTO empleado (id, nombre, apellido, fecha_nacimiento, usuario, password, rol, id_jefe, admin)
                    VALUES (" . intval($empleado['idEmpleado']) . ", '" . $empleado['nombre'] . "', '" . $empleado['apellido'] . "',
                    '" . date($empleado['fechaNacimiento']) . "', '" . $empleado['usuario'] . "', '" . $empleado['clave'] . "', '"
                . $empleado['rol'] . "', " . intval($empleado['idJefe'] ). ", " . true . ")";
            // var_dump($sql);
            // die;
            $this->executeQuery($sql);
        }
    }

    private function executeQuery($sql)
    {
        $servername = "localhost";
        $username = "admin";
        $password = "admin";
        $database = "incentivos";
        $conn = new \mysqli($servername, $username, $password, $database);
        return $conn->query($sql);
    }
}
