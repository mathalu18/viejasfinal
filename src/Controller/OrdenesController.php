<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class OrdenesController extends AbstractController
{
    /**
     * @Route("/ordenes", name="ordenes")
     */
    public function index()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $this->agregarOrdenes($data);
        return $this->render('ordenes/index.html.twig', [
            'controller_name' => 'OrdenesController',
        ]);
    }

    private function agregarOrdenes($ordenes)
    {

        $servername = "localhost";
        $username = "admin";
        $password = "admin";
        $database = "incentivos";
        $conn = new \mysqli($servername, $username, $password, $database);
        $this->executeQuery('TRUNCATE TABLE `producto_orden`');
        $this->executeQuery('TRUNCATE TABLE `orden`');
        foreach ($ordenes as $orden){
            foreach ($orden['ordenes'] as $data) {
                $sql = "INSERT INTO orden (id, id_empleado, total_orden, estado) VALUES (". intval($data['idOrden']) .", ". intval($orden['idEmpleado']) .", ". intval($data['totalOrden']) .", '". $data['estado'] ."')";
                $this->executeQuery($sql);
                foreach ($data['productos'] as $valor) {
                    $sql = "INSERT INTO producto_orden (id_orden, id_producto, cantidad) VALUES (". intval($data['idOrden']) .", ". intval($valor['idProducto']) .", ". intval($valor['cantidad']) .")";
                    $this->executeQuery($sql);
                }
            }
        }
    }

    private function executeQuery($sql)
    {
        $servername = "localhost";
        $username = "admin";
        $password = "admin";
        $database = "incentivos";
        $conn = new \mysqli($servername, $username, $password, $database);
        return $conn->query($sql);
    }
}
