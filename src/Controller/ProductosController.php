<?php

namespace App\Controller;

use PhpParser\Node\Expr\Print_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProductosController extends AbstractController
{
    /**
     * @Route("/productos", name="productos")
     */
    public function index()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        // echo '<pre>';
        // print_r($data);
        $this->crearProductos($data);
        return $this->render('productos/index.html.twig', [
            'controller_name' => 'ProductosController',
        ]);
    }
    public function crearProductos($productos)
    {
        $servername = "localhost";
        $username = "admin";
        $password = "admin";
        $database = "incentivos";
        $conn = new \mysqli($servername, $username, $password, $database);
        $this->executeQuery('TRUNCATE TABLE `producto`');
        foreach ($productos as $data) {

            $sql = "INSERT INTO producto (id, nombre, descripcion, url_imagen, puntos) VALUES (" . intval($data['codProducto']) . ", '" . $data['nombreProducto'] . "', '" . $data['descripcionProducto'] . "', '" . $data['urlImagen'] . "', " . intval($data['puntosProducto']) . ")";
            // var_dump($sql);
            // die;
            $this->executeQuery($sql);
        }
    }
    private function executeQuery($sql)
    {
        $servername = "localhost";
        $username = "admin";
        $password = "admin";
        $database = "incentivos";
        $conn = new \mysqli($servername, $username, $password, $database);
        return $conn->query($sql);
    }
}
