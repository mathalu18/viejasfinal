<?php

namespace App\Controller;

use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProcesamientoPuntosController extends AbstractController
{
    /**
     * @Route("/procesamiento-puntos", name="procesamiento_puntos")
     */
    public function index()
    {
        return $this->render('procesamiento_puntos/index.html.twig', [
            'controller_name' => 'ProcesamientoPuntosController',
        ]);
    }

    /**
     * @Route("/procesar-puntos-empleados", name="consultar-empleados")
     */
    public function consultarEmpleados()
    {
        $sql = 'SELECT e.id, UPPER(e.rol) as rol FROM empleado e 
                INNER JOIN sesion s ON e.id = s.id';
        $empleado = self::consutaQuery($sql);
        if (!empty($empleado)) {
            $empleado = $empleado[0];
            $id = $empleado['id'];
            switch ($empleado['rol']) {
                case 'SUPERVISOR':
                    $sql = 'SELECT em.nombre as empleado, p.descripcion, p.monto_base, SUM(valor_venta) as ventas FROM venta v
                        INNER JOIN periodos p ON v.id_periodo = p.id
                        INNER JOIN empleado em ON v.id_empleado = em.id
                        WHERE em.id_jefe = ' . $id . '
                        GROUP BY p.id, v.id_empleado';
                    break;
                case 'EMPLEADO':
                    $sql = 'SELECT em.nombre as empleado, p.descripcion, p.monto_base, SUM(valor_venta) as ventas FROM venta v
                        INNER JOIN periodos p ON v.id_periodo = p.id
                        INNER JOIN empleado em ON v.id_empleado = em.id
                        WHERE v.id_empleado = ' . $id . '
                        GROUP BY p.id';
                    break;
            }
            $ventas_puntos = self::consutaQuery($sql);
            $puntos = [];
            $contador = 0;
            $total_puntos = 0;
            if (!empty($ventas_puntos)) {
                foreach ($ventas_puntos as $venta) {
                    $puntos[$contador]['empleado'] = $venta['empleado'];
                    $puntos[$contador]['descripcion'] = $venta['descripcion'];
                    $puntos[$contador]['monto_base'] = $venta['monto_base'];
                    $puntos[$contador]['ventas'] = $venta['ventas'];
                    $porcentaje = (100 * $venta['ventas']) / $venta['monto_base'];
                    $puntos[$contador]['porcentaje'] = $porcentaje;
                    if ($porcentaje >= 100) {
                        if ($empleado['rol'] == 'SUPERVISOR') {
                            $puntos[$contador]['puntos'] = 12000;
                            $total_puntos += 12000;
                        } else {
                            $puntos[$contador]['puntos'] = 10000;
                            $total_puntos += 10000;
                        }
                    } elseif ($porcentaje <= 99.9999999 and $porcentaje >= 50) {
                        if ($empleado['rol'] == 'SUPERVISOR') {
                            $puntos[$contador]['puntos'] = 6000;
                            $total_puntos += 6000;
                        } else {
                            $puntos[$contador]['puntos'] = 5000;
                            $total_puntos += 5000;
                        }
                    } elseif ($porcentaje <= 49.9999999 and $porcentaje >= 20 and $empleado['rol'] == 'SUPERVISOR') {
                        $puntos[$contador]['puntos'] = 2000;
                        $total_puntos += 2000;
                    } else {
                        $puntos[$contador]['puntos'] = 0;
                    }
                    $contador++;
                }
                if ($empleado['rol'] == 'SUPERVISOR') {
                    $sql = 'SELECT COUNT(id) as cantidad FROM empleado WHERE id_jefe = ' . $id;
                    $cantidad_empleados = self::consutaQuery($sql)[0]['cantidad'];
                    if (!empty($cantidad_empleados)) {
                        $puntos['promedio'] = $total_puntos / $cantidad_empleados;
                    } else {
                        $puntos['promedio'] = 0;
                    }
                } else {
                    $puntos['promedio'] = $total_puntos;
                }
            } else {
                $puntos = [];
            }
        } else {
            $puntos = [];
        }
        die(json_encode($puntos, true));
    }

    private function executeQuery($sql)
    {
        $servername = "localhost";
        $username = "admin";
        $password = "admin";
        $database = "viejastecnologias";
        $conn = new \mysqli($servername, $username, $password, $database);
        return $conn->query($sql);
    }

    private function consutaQuery($sql)
    {
        $data = [];
        $servername = "localhost";
        $username = "admin";
        $password = "admin";
        $database = "viejastecnologias";
        $conn = new \mysqli($servername, $username, $password, $database);
        $resultado = $conn->query($sql);
        while ($row = $resultado->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }
}
