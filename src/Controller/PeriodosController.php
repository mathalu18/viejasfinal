<?php

namespace App\Controller;

use App\Entity\Periodos;
use App\Form\PeriodosType;
use App\Repository\PeriodosRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/periodo")
 */
class PeriodosController extends AbstractController
{
    /**
     * @Route("/", name="periodos_index", methods={"GET"})
     */
    public function index(PeriodosRepository $periodosRepository): Response
    {
        return $this->render('periodos/index.html.twig', [
            'periodos' => $periodosRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="periodos_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $periodo = new Periodos();
        $form = $this->createForm(PeriodosType::class, $periodo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($periodo);
            $entityManager->flush();

            return $this->redirectToRoute('periodos_index');
        }

        return $this->render('periodos/new.html.twig', [
            'periodo' => $periodo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="periodos_show", methods={"GET"})
     */
    public function show(Periodos $periodo): Response
    {
        return $this->render('periodos/show.html.twig', [
            'periodo' => $periodo,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="periodos_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Periodos $periodo): Response
    {
        $form = $this->createForm(PeriodosType::class, $periodo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('periodos_index');
        }

        return $this->render('periodos/edit.html.twig', [
            'periodo' => $periodo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="periodos_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Periodos $periodo): Response
    {
        if ($this->isCsrfTokenValid('delete'.$periodo->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($periodo);
            $entityManager->flush();
        }

        return $this->redirectToRoute('periodos_index');
    }
}
