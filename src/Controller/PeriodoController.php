<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PeriodoController extends AbstractController
{
    /**
     * @Route("/periodos", name="periodo")
     */
    public function index()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        // echo '<pre>';
        // print_r($data);
        $this->crearPeriodos($data);
        return $this->render('periodo/index.html.twig', [
            'controller_name' => 'PeriodoController',
        ]);
    }
    private function crearPeriodos($periodos)
    {
        $servername = "localhost";
        $username = "admin";
        $password = "admin";
        $database = "incentivos";
        $conn = new \mysqli($servername, $username, $password, $database);
        $this->executeQuery('TRUNCATE TABLE `periodos`');
        foreach ($periodos as $data) {

            $sql = "INSERT INTO periodos (id, descripcion, monto_base) VALUES ('" . $data['idPeriodo'] . "', '" . $data['descripcion'] . "', '" . $data['montoBase'] . "')";
            // var_dump($sql);
            // die;
            $this->executeQuery($sql);
        }
    }


    private function executeQuery($sql)
    {
        $servername = "localhost";
        $username = "admin";
        $password = "admin";
        $database = "incentivos";
        $conn = new \mysqli($servername, $username, $password, $database);
        return $conn->query($sql);
    }
}
