<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ResetDatabseController extends AbstractController
{
    /**
     * @Route("/reset-database", name="reset_databse")
     */
    public function index()
    {
        $this->eliminarTabla();
        return $this->render('reset_databse/index.html.twig', [
            'controller_name' => 'ResetDatabseController',
        ]);
    }

    private function eliminarTabla()
    {

        $array = array(
            "DROP TABLE orden",
            "DROP TABLE periodos",
            "DROP TABLE producto_orden",
            "DROP TABLE venta",
            "DROP TABLE puntos",
            "DROP TABLE empleado",
            "DROP TABLE producto",
            "DROP TABLE sesion"
        );

        $servername = "localhost";
        $username = "admin";
        $password = "admin";
        $database = "incentivos";
        $conn = new \mysqli($servername, $username, $password, $database);

        foreach ($array as $valor) {
            $this->executeQuery($valor);
        }
    }

    private function executeQuery($sql)
    {
        $servername = "localhost";
        $username = "admin";
        $password = "admin";
        $database = "incentivos";
        $conn = new \mysqli($servername, $username, $password, $database);
        return $conn->query($sql);
    }
}
