<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class VentasController extends AbstractController
{
    /**
     * @Route("/ventas", name="ventas")
     */
    public function index()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $this->agregarVentas($data);
        return $this->render('ventas/index.html.twig', [
            'controller_name' => 'VentasController',
        ]);
    }

    private function agregarVentas($ventas)
    {

        $servername = "localhost";
        $username = "admin";
        $password = "admin";
        $database = "incentivos";
        $conn = new \mysqli($servername, $username, $password, $database);
        $this->executeQuery('TRUNCATE TABLE `venta`');
        foreach ($ventas as $venta) {
            foreach ($venta as $data) {
                foreach ($data['ventas'] as $valor) {
                    $sql = "INSERT INTO venta (id_empleado, id_periodo, valor_venta) VALUES (" . intval($data['idVendedor']) . ", '". $valor['idPeriodo'] ."', ". intval($valor['valorVenta']) .")";
                    $this->executeQuery($sql);
                }
            }
        }
    }

    private function executeQuery($sql)
    {
        $servername = "localhost";
        $username = "admin";
        $password = "admin";
        $database = "incentivos";
        $conn = new \mysqli($servername, $username, $password, $database);
        return $conn->query($sql);
    }
}
