<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IngresoController extends AbstractController
{
    /**
     * @Route("/ingreso", name="ingreso")
     */
    public function index()
    {
        return $this->render('ingreso/index.html.twig');
    }

    /**
     * @Route("/validar-ingreso", name="validar-ingreso")
     */
    public function validarIngreso()
    {
        $data = $_POST;
        $sql = 'SELECT id FROM empleado
                WHERE usuario = "' . $data['usuario'] . '" AND 
                password = "' . $data['password'] . '" ORDER BY nombre';
        $empleado = self::consutaQuery($sql);
        if (!empty($empleado)) {
            $sql = 'TRUNCATE TABLE sesion';
            self::executeQuery($sql);
            $sql = 'INSERT INTO sesion (id) VALUES (' . $empleado[0]['id'] . ')';
            self::executeQuery($sql);
            return $this->render('dash_board/index.html.twig');
        } else {
            return $this->render('ingreso/index.html.twig');
        }
    }

    private function consutaQuery($sql)
    {
        $data = [];
        $servername = "localhost";
        $username = "admin";
        $password = "admin";
        $database = "incentivos";
        $conn = new \mysqli($servername, $username, $password, $database);
        $resultado = $conn->query($sql);
        while ($row = $resultado->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }
}
